init();

function createRandomizedBinaryMatrix(width, height) {
  var matrix = [];

  for (var i = 0; i < width; i++) {
    var row = [];
    for (var j = 0; j < height; j++) {
      row[j] = getRandomizedBinaryNumber();
    }
    matrix[i] = row;
  }

  return matrix;
}

function createBinaryMatrix(width, height) {
  var matrix = [];

  for (var i = 0; i < width; i++) {
    for (var j = 0; j < height; j++) {
      matrix[i] = [];
    }
  }

  return matrix;
}

function getRandomizedBinaryNumber() {
  return Math.round(Math.random());
}

function init() {
  var canvas = document.getElementById("canvas");
  canvas.width = document.body.clientWidth; //document.width is obsolete
  canvas.height = document.body.clientHeight; //document.height is obsolete
  var canvasW = canvas.width;
  var canvasH = canvas.height;
  var ctx = canvas.getContext("2d");
  ctx.scale(5, 5);

  console.log(canvasH);
  console.log(canvasW);
  if (canvas.getContext) {
    runGameOfLife(canvasW, canvasH);
  }
}

function drawGrid(matrix, width, height) {
  var c = document.getElementById("canvas");
  var ctx = c.getContext("2d");

  ctx.clearRect(0, 0, width, height);

  console.log(width, height);
  for (var j = 0; j < width; j++) {
    for (var k = 0; k < height; k++) {
      if (matrix[j][k] == 1) {
        ctx.fillStyle = "#468499";

        ctx.fillRect(j, k, 1, 1);
      }
    }
  }
}

function computeGenerationalChange(matrix, width, height) {
  var newMatrix = createBinaryMatrix(width, height);
  for (var j = 0; j < width; j++) {
    for (var k = 0; k < height; k++) {
      numberOfNeighbors = calculateNumberOfNeighbors(matrix, j, k, width, height);

      if (matrix[j][k] == 1) {
        if (numberOfNeighbors < 2 || numberOfNeighbors > 3) {
          newMatrix[j][k] = 0;
        } else if (numberOfNeighbors == 2 || numberOfNeighbors == 3) {
          newMatrix[j][k] = 1;
        }
      } else if (numberOfNeighbors == 3) {
        newMatrix[j][k] = 1;
      }
    }
  }
  return newMatrix;
}

function calculateNumberOfNeighbors(matrix, j, k, width, height) {
  var rowLimit = width - 1;
  var columnLimit = height - 1;
  var numberOfNeighbors = 0;

  for (var w = Math.max(0, j - 1); w <= Math.min(j + 1, rowLimit); w++) {
    for (var h = Math.max(0, k - 1); h <= Math.min(k + 1, columnLimit); h++) {
      if (matrix[w][h] == 1 && !(w == j && h == k)) {
        numberOfNeighbors++;
      }
    }
  }

  return numberOfNeighbors;
}

function runGameOfLife(width, height) {
  var matrix = createRandomizedBinaryMatrix(width, height);
  drawGrid(matrix, width, height);
  var x = 0;

  setInterval(function() {
    matrix = computeGenerationalChange(matrix, width, height);
    drawGrid(matrix, width, height);
  }, 1000);
}
